<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    protected $file = 'users2018.xlsx';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader("Xlsx");
        $spreadsheet = $reader->load($this->file);
        $users = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        foreach ($users as $userFromTable) {
            if ($userFromTable['A']) {
                $user = new \App\Models\User([
                    'username' => $userFromTable['A'],
                    'password' => bcrypt($userFromTable['B']),
                    'full_name' => $userFromTable['F']
                ]);
                if (!str_contains($userFromTable['A'], 'contestant')) {
                    $user->role = 'jury';
                }
                $user->save();
            }

        }
    }
}
