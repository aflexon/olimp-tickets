<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Models\Comment;
use App\Models\Message;
use Illuminate\Http\Request;
use Auth;
use Session;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:jury']);
    }

    public function store(Message $message, CommentRequest $request)
    {
        if ($message->comments()->count() > 1) {
            Session::flash('status', 'Кто-то уже ответил на этот вопрос!');
            return back();
        }

        $comment = new Comment(['body' => $request->body]);
        $comment->message()->associate($message);
        $comment->author()->associate(Auth::user()->id);
        $comment->save();
        $comment->message->touch();
        Session::flash('status', 'Ответ на вопрос успешно добавлен!');
        return redirect()->route('home');
    }

    public function destroy(Message $message, Comment $comment)
    {
        $comment->delete();
        Session::flash('status', 'Ответ успешно удален');
        return back();
    }
}
