<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Models\Comment;
use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use Session;

class MessageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:jury')->only('destroy');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $messages = Auth::user()->availableMessages()->with('comments', 'comments.author')->get();
        $unansweredMessages = Auth::user()->availableMessages()->has('comments', '<=', 1)
            ->where('type', 'question')->count();
        return view('home')->with(compact('users', 'messages', 'unansweredMessages'));
    }

    /**
     *
     * @param CommentRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request)
    {
        $comment = new Comment(['body' => $request->body]);
        $comment->author()->associate(Auth::user()->id);

        $message = new Message();
        $message->sender()->associate(Auth::user()->id);
        $message->recipient_id = 0;
        if (Auth::user()->role === 'jury') {
            $message->type = 'message';
            $message->recipient_id = $request->recipient_id;
        }
        $message->save();

        $comment->message()->associate($message);
        $comment->save();
        Session::flash('status', 'Отправлено успешно!');
        return redirect()->route('home');
    }

    public function destroy(Message $message)
    {
        $message->delete();
        Session::flash('status', 'Сообщение/Вопрос успешно удалено');
        return back();
    }
}
