<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['body'];

    protected $touches = ['message'];

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function message()
    {
        return $this->belongsTo(Message::class);
    }
}
