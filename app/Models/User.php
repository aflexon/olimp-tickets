<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'full_name', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function availableMessages()
    {
        $query = Message::query()->latest();
        // ->orderBy('updated_at', 'desc')
        if ($this->role === 'jury') {
            return $query;
        }

        $query->where('sender_id', $this->id)
            ->orWhere(function ($query) {
                $query->where('type', 'message')->where(function ($query) {
                    $query->where('recipient_id', 0)->orWhere('recipient_id', $this->id);
                });
            });
        return $query;
    }

    public function getDisplayNameAttribute()
    {
        if (Auth::user()->role === 'participant' && $this->role === 'jury') {
            return 'Жюри';
        }
        return $this->full_name;
    }
}
