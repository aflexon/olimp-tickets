<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [];

    public function comments() {
        return $this->hasMany(Comment::class);
    }

    public function sender() {
        return $this->belongsTo(User::class, 'sender_id');
    }

    public function recipient() {
        return $this->belongsTo(User::class, 'recipient_id');
    }

    public function getQuestionAttribute()
    {
        return $this->comments->first();
    }

    public function getAnswerAttribute()
    {
        if ($this->comments->count() < 2) {
            return null;
        }
        return $this->comments->last();
    }
}
