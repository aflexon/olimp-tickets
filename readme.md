## Olimp Tickets System

Simple communication system between participants and jury

## Requirements

* php 7
* mysql

## Installation

Copy .env.example to .env, change parameters in .env

```
cp .env.example .env
```

Install composer dependencies
```
composer install
```

Generate app key
```
php artisan key:generate
```


Run db migrations
```
php artisan migrate
```
