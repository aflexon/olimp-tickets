<div class="media">
    @isset ($title)
        <div class="media-left">
            {{$title}}
        </div>
    @endisset
    <div class="media-body">
        <h5 class="media-heading">
            <span>
                <strong>{{ $comment->author->display_name }}</strong>
            </span>
            <br>
            <small>
                {{ $comment->created_at->format('d.m.Y H:i:s') }}
                @isset ($delete)
                @if ($delete && Auth::user()->role === 'jury')
                    <a href="{{route('comment.destroy', ['message' => $comment->message->id, 'comment' => $comment->id])}}"
                       data-method="delete"
                       data-confirm="Вы уверены что хотите удалить ответ на вопрос #{{ $comment->message->id }}?"
                    >
                        Удалить ответ
                    </a>
                @endif
                @endisset
            </small>
        </h5>
        <p>{{ $comment->body }}</p>
        {{ $slot }}
    </div>
</div>