<div class="panel panel-default">
    <div class="panel-heading">Отправить сообщение</div>

    <div class="panel-body">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <form class="form-horizontal" method="POST" action="{{ route('message.store') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('recipient_id') ? ' has-error' : '' }}">
                <label for="recipient" class="col-md-3 control-label">Получатель</label>

                <div class="col-md-9">
                    <select class="form-control" name="recipient_id" id="recipient">
                        <option value="0">Все участники</option>
                        @foreach($users as $user)
                            <option value="{{ $user->id }}">{{ $user->full_name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('recipient_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('recipient_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                <label for="body" class="col-md-3 control-label">Сообщение</label>

                <div class="col-md-9">
                    <textarea class="form-control" name="body" id="body"></textarea>

                    @if ($errors->has('body'))
                        <span class="help-block">
                            <strong>{{ $errors->first('body') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-9 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">
                        Отправить сообщение
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>