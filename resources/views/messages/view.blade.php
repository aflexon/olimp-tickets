<div class="panel panel-info">
    <div class="panel-heading">
        <span>
            {{ $message->recipient_id === 0 ? 'Сообщение' : 'Персональное сообщение' }}
            #{{ $message->id }}
            @if (Auth::user()->role === 'jury')
                <a href="{{route('message.destroy', ['message' => $message->id])}}"
                   data-method="delete"
                   data-confirm="Вы уверены что хотите удалить сообщение #{{ $message->id }}?"
                >
                    Удалить
                </a>
            @endif
        </span>
        <span class="pull-right text-muted">{{ $message->created_at->format('d.m.Y H:i:s') }}</span>
    </div>

    <div class="panel-body">
        @if (Auth::user()->role === 'jury')
            <p><strong>Получатель:</strong> {{ $message->recipient->display_name ??  'Все участники' }}</p>
        @endif
        @forelse ($message->comments as $comment)
            @component('comments.view', ['comment' => $comment])
            @endcomponent
        @empty
            <p>Что-то не так с системой чат жюри!</p>
        @endforelse
    </div>
</div>