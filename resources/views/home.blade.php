@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @if(Auth::user()->role === 'jury' && $unansweredMessages > 0)
                <div class="alert alert-danger" role="alert">
                    Внимание! Количество вопросов без ответа: {{ $unansweredMessages }}
                </div>
            @endif

            @if(Auth::user()->role === 'participant')
                @include('questions/create')
            @endif

            @if(Auth::user()->role === 'jury')
                @include('messages/create')
            @endif

            @forelse($messages as $message)
                @if($message->type === 'message')
                    @include('messages/view', compact($message))
                @elseif($message->type === 'question')
                    @include('questions/view', compact($message))
                @endif
            @empty
                <div class="panel panel-default">
                    <div class="panel-heading">Чат с жюри</div>
                    <div class="panel-body">
                        <p>В системе пока еще нет сообщений</p>
                    </div>
                </div>
            @endforelse
        </div>
    </div>
</div>
@endsection
