<div class="panel panel-default">
    <div class="panel-heading">
        <span>Вопрос #{{ $message->id }}</span>
        <span class="pull-right text-muted">{{ $message->created_at->format('d.m.Y H:i:s') }}</span>
    </div>

    <div class="panel-body">
        @if ( $message->question )
            @component('comments.view', ['comment' => $message->question])
                @if ($message->answer)
                    @component('comments.view', ['comment' => $message->answer, 'delete' => true])
                        @slot('title')
                            <img src="/images/envelope.png" width="32" height="32" class="media-object">
                        @endslot
                    @endcomponent
                @elseif (Auth::user()->role === 'jury')
                    <div class="media">
                        <div class="media-left">
                            <img src="/images/envelope.png" width="32" height="32" class="media-object">
                        </div>
                        <div class="media-body">

                            <h5 class="media-heading">
                                <strong>Жюри</strong>
                            </h5>
                            <form class="form-horizontal" method="POST" action="{{ route('comment.store', ['message' => $message->id]) }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                                    <div class="col-md-12">
                                        <textarea class="form-control input-body" name="body"></textarea>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                                    <div class="col-md-12">
                                        Быстрые ответы:
                                        <a href="javascript://"
                                           onclick="this.parentElement.parentElement.parentElement.querySelector('textarea.input-body').innerHTML = 'Да'
                                            "
                                        >Да</a>
                                        <a href="javascript://"
                                           onclick="this.parentElement.parentElement.parentElement.querySelector('textarea.input-body').innerHTML = 'Нет' "
                                        >Нет</a>
                                        <a href="javascript://"
                                           onclick="this.parentElement.parentElement.parentElement.querySelector('textarea.input-body').innerHTML = 'Без комментариев' "
                                        >Без комментариев</a>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary">
                                            Ответить
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                @else
                    <strong>Жюри пока еще не ответило на этот вопрос</strong>
                @endif

            @endcomponent
        @endif
    </div>
</div>