<div class="panel panel-default">
    <div class="panel-heading">Задать вопрос</div>

    <div class="panel-body">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <p>
            Старайтесь сформулировать вопрос так, чтобы жюри могло дать ответ «да» или «нет». В противном случае Вы рискуете получить ответ «без комментариев».
        </p>

        <p>
            Если вопрос связан с технической неисправностью, не забудьте указать номер своего рабочего места.
        </p>

        <p>
            Если вопрос связан с задачей, не забудьте указать номер или название задачи.
        </p>
        <form class="form-horizontal" method="POST" action="{{ route('message.store') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                <label for="body" class="col-md-2 control-label">Вопрос</label>

                <div class="col-md-10">
                    <textarea class="form-control" name="body" id="body"></textarea>

                    @if ($errors->has('body'))
                        <span class="help-block">
                            <strong>{{ $errors->first('body') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">

                <div class="col-md-10 col-md-offset-2">
                    <button type="submit" class="btn btn-primary">
                        Задать вопрос
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>