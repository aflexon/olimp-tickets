@extends('layouts.app')

@section('content')
<div class="col">
    <div class="cell">
        <div class="col">
            @if(Auth::user()->role === 'jury' && $unansweredMessages > 0)
                <div class="cell panel" role="alert">
                    <div class="body cell">
                        <div class="color-red center">
                            <div class="cell">
                                Внимание! Количество вопросов без ответа: {{ $unansweredMessages }}
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @if(Auth::user()->role === 'participant')
                @include('questions/create')
            @endif

            @if(Auth::user()->role === 'jury')
                @include('messages/create')
            @endif

            @forelse($messages as $message)
                @if($message->type === 'message')
                    @include('messages/view', compact($message))
                @elseif($message->type === 'question')
                    @include('questions/view', compact($message))
                @endif
            @empty
                <div class="panel cell">
                    <div class="header">Чат с жюри</div>
                    <div class="body">
                        <div class="cell">
                            <p>В системе пока еще нет сообщений</p>
                        </div>
                    </div>
                </div>
            @endforelse
        </div>
    </div>
</div>
@endsection
