@extends('layouts.app')

@section('content')
<div class="col">
    <div class="col width-1of4"></div>
    <div class="col width-1of2">
        <div class="cell panel">
            <div class="header">Войти в систему</div>

            <div class="body">
                <div class="cell">
                    <form class="col" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="col">
                            <div class="col width-1of3">
                                <div class="cell">
                                    <label for="username">Имя пользователя</label>
                                </div>
                            </div>
                            <div class="col width-fill">
                                <div class="cell">
                                    <input id="username" type="text" class="text" name="username" value="{{ old('username') }}">
                                </div>
                            </div>
                            @if ($errors->has('username'))
                                <div class="color-red cell">
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                </div>
                            @endif
                        </div>

                        <div class="col">
                            <div class="col width-1of3">
                                <div class="cell">
                                    <label for="password">Пароль</label>
                                </div>
                            </div>

                            <div class="col width-fill">
                                <div class="cell">
                                    <input id="password" type="password" class="text" name="password">
                                </div>
                            </div>
                            @if ($errors->has('password'))
                                <div class="cell color-red">
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                </div>
                            @endif
                        </div>

                        <div class="coll">
                            <div class="col width-1of3"></div>
                            <div class="col width-fill">
                                <div class="cell">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Запомнить меня
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col">
                            <div class="col width-1of3"></div>
                            <div class="col width-fill">
                                <div class="cell">
                                    <button type="submit" class="button">
                                        Войти
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
