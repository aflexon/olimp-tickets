<div class="panel cell">
    <div class="header">
        <span>
            {{ $message->recipient_id === 0 ? 'Сообщение' : 'Персональное сообщение' }}
            #{{ $message->id }}
            @if (Auth::user()->role === 'jury')
                <a href="{{route('message.destroy', ['message' => $message->id])}}"
                   data-confirm="Вы уверены что хотите удалить сообщение #{{ $message->id }}?"
                   data-method="delete"
                >
                    Удалить
                </a>
            @endif
        </span>
        <span class="pull-right tiny">{{ $message->created_at->format('d.m.Y H:i:s') }}</span>
    </div>

    <div class="body">
        <div class="cell">
            @if (Auth::user()->role === 'jury')
                <p><strong>Получатель:</strong> {{ $message->recipient->display_name ??  'Все участники' }}</p>
            @endif
            @forelse ($message->comments as $comment)
                @component('comments.view', ['comment' => $comment])
                @endcomponent
            @empty
                <p>Что-то не так с системой чат жюри!</p>
            @endforelse
        </div>
    </div>
</div>