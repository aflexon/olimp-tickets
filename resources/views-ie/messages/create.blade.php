<div class="panel cell">
    <div class="header">Отправить сообщение</div>

    <div class="body">
        <div class="cell">
            @if (session('status'))
                <div class="cell panel" role="alert">
                    <div class="body cell">
                        <div class="color-green center">
                            <div class="cell">
                                {{ session('status') }}
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <form class="col" method="POST" action="{{ route('message.store') }}">
                {{ csrf_field() }}

                <div class="col {{ $errors->has('recipient_id') ? ' color-red' : '' }}">
                    <div class="col width-1of5">
                        <div class="cell">
                            <label for="recipient">Получатель</label>
                        </div>
                    </div>

                    <div class="col width-fill">
                        <div class="cell">
                            <select name="recipient_id" id="recipient" style="width: 90%">
                                <option value="0">Все участники</option>
                                @foreach($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->full_name }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('recipient_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('recipient_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col {{ $errors->has('body') ? ' color-red' : '' }}">
                    <div class="col width-1of5">
                        <div class="cell">
                            <label for="body">Сообщение</label>
                        </div>
                    </div>

                    <div class="col width-fill">
                        <div class="cell">
                            <textarea name="body" id="body" style="width: 90%"></textarea>

                            @if ($errors->has('body'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('body') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="col width-1of5"></div>
                    <div class="col width-fill">
                        <div class="cell">
                            <button type="submit" class="btn btn-primary">
                                Отправить сообщение
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>