<div class="panel cell">
    <div class="header">
        <span>Вопрос #{{ $message->id }}</span>
        <span class="pull-right tiny">{{ $message->created_at->format('d.m.Y H:i:s') }}</span>
    </div>

    <div class="body">
        <div class="cell">
            @if ( $message->question )
                @component('comments.view', ['comment' => $message->question])
                    @if ($message->answer)
                        @component('comments.view', ['comment' => $message->answer, 'delete' => true])
                            @slot('title')
                                <img src="/images/envelope.png" width="32" height="32" class="media-object">
                            @endslot
                        @endcomponent
                    @elseif (Auth::user()->role === 'jury')
                        <div class="meida">
                            <div class="col width-fit">
                                <img src="/images/envelope.png" width="32" height="32" class="media-object">
                            </div>
                            <div class="col width-fill">

                                <h5 class="media-heading">
                                    <strong>Жюри</strong>
                                </h5>
                                <form class="form-horizontal" method="POST" action="{{ route('comment.store', ['message' => $message->id]) }}">
                                    {{ csrf_field() }}

                                    <div class="col {{ $errors->has('body') ? ' color-red' : '' }}">
                                        <div class="col-md-12">
                                            <textarea id="comment-body-{{$message->id}}" style="width: 90%" name="body"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('body') ? ' color-red' : '' }}">
                                        <div class="col-md-12">
                                            Быстрые ответы:
                                            <a href="javascript://"
                                               onclick="document.getElementById('comment-body-{{$message->id}}').innerHTML = 'Да'"
                                            >
                                                Да
                                            </a>
                                            <a href="javascript://"
                                               onclick="document.getElementById('comment-body-{{$message->id}}').innerHTML = 'Нет'"
                                            >
                                                Нет
                                            </a>
                                            <a href="javascript://"
                                               onclick="document.getElementById('comment-body-{{$message->id}}').innerHTML = 'Без комментариев'"
                                            >
                                                Без комментариев
                                            </a>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary">
                                                Ответить
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    @else
                        <strong>Жюри пока еще не ответило на этот вопрос</strong>
                    @endif

                @endcomponent
            @endif
        </div>
    </div>
</div>