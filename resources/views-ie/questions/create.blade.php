<div class="cell panel">
    <div class="header">Задать вопрос</div>

    <div class="body">
        <div class="cell">
            @if (session('status'))
                <div class="cell panel" role="alert">
                    <div class="body cell">
                        <div class="color-green center">
                            <div class="cell">
                                {{ session('status') }}
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <p>
                Старайтесь сформулировать вопрос так, чтобы жюри могло дать ответ «да» или «нет». В противном случае Вы рискуете получить ответ «без комментариев».
            </p>

            <p>
                Если вопрос связан с технической неисправностью, не забудьте указать номер своего рабочего места.
            </p>

            <p>
                Если вопрос связан с задачей, не забудьте указать номер или название задачи.
            </p>
            <form class="col" method="POST" action="{{ route('message.store') }}">
                {{ csrf_field() }}

                <div class="col">
                    <div class="col width-1of4">
                        <div class="cell">
                            <label for="body">Вопрос</label>
                        </div>
                    </div>

                    <div class="col width-fill">
                        <div class="cell">
                            <textarea name="body" id="body" style="width: 90%"></textarea>
                            @if ($errors->has('body'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('body') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="col width-1of4"></div>
                    <div class="col width-fill">
                        <div class="cell">
                            <button type="submit" class="btn btn-primary">
                                Задать вопрос
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>