<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" media="all"  href="/css/cascade/production/build-full.min.css" />
    <link rel="stylesheet" type="text/css" media="all"  href="/css/site.css" />
    <!--[if lt IE 8]><link rel="stylesheet" href="/css/cascade/production/icons-ie7.min.css"><![endif]-->
    <!--[if lt IE 9]><script src="/js/shim/iehtmlshiv.js"></script><![endif]-->

    {{--<meta http-equiv="refresh" content="30">--}}
</head>
<body class="narrow">
    <div class="site-center" id="app">
        <div class="site-body">
            <div class="site-header">
                <div class="col width-fill sitemenu">
                    <div class="col width-fill mobile-width-fill">
                        <div class="cell">
                            <ul class="nav">
                                <li>
                                    <a href="{{ url('/') }}">
                                        {{ config('app.name', 'Laravel') }}
                                    </a>
                                </li>
                                @guest
                                <li><a href="{{ route('login') }}">Войти</a></li>
                                {{--<li><a href="{{ route('register') }}">Register</a></li>--}}
                                @else
                                    <li><a href="#">{{ Auth::user()->full_name }}</a></li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault ? event.preventDefault() : (event.returnValue = false);
                                                 document.getElementById('logout-form').submit();">
                                            Выйти
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                    @endguest
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col">
                @yield('content')
            </div>
        </div>


        <div class="site-footer" style="text-align:center">
            <div class="cell">
                &copy; {{ date('Y') }} {{ config('app.name') }}. Бета-версия
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/laravel-ie.js') }}"></script>
</body>
</html>
