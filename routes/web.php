<?php

Auth::routes();

Route::get('/', 'MessageController@index')->name('home');
Route::post('/message', 'MessageController@store')->name('message.store');
Route::delete('/message/{message}', 'MessageController@destroy')->name('message.destroy');
Route::post('/message/{message}/comment', 'CommentController@store')->name('comment.store');
Route::delete('/message/{message}/comment/{comment}', 'CommentController@destroy')->name('comment.destroy');
